# ES2 - Livraria

<ul>
<li>
<strong> Projeto da aula: Construção de uma livraria.</strong>
</li>
</ul>

<h3>Autores</h3>
<ul>
<li><a href="https://gitlab.com/cleoreggio">Cléo Reggio</li>
<li><a href="https://gitlab.com/pablovmartins">Pablo Martins</li>
<li><a href="https://gitlab.com/wagnerandersson">Wagner anderson</li>
<li><a href="http://gitlab.com/W-araujo">Wesley Araujo</li>
</ul>

<h1>⬇ Technologies</h1>

<h4>Back-end</h4>
<ul>
  <li>typescript</li>
  <li>ts-node</li>
  <li>pront-sync</li>
  </ul>

<h1>⬇ Features</h1>
   <ul>
        <li>(Back-end) Cadastra, remove e altera livros</li>
        <li>(Back-end) Cadastra capitulos</li>
        <li>(Back-end) Cadastra textos</li>
        <li>(Back-end) Cadastra autores</li>
        <li>(Back-end) Mostra a listagem dos livros</li>
    </ul>
    
<h1>⬇ Installation </h1>
<h4>Você precisa instalar o Node.js antes de tudo, para depois clonar o projeto via HTTPS ou SSH. Rode esse comando:</h4>

<strong>HTTP</strong>
<p><code>git clone https://gitlab.com/PabloVMartins/es2-livraria.git</code></p>
<p>Ou</p>
<strong>SSH</strong>
<p><code>git clone git@gitlab.com:PabloVMartins/es2-livraria.git</code></p>

<h4>Agora falta instalar as dependências. Rode esse comando:</h4>

<p><code>npm install</code></p>

<h1>⬇ Getting Started </h1>

<h4>Back-end</h4>
  <ul>
       <li>
        Navegue entre as pastas ate chegar dentro "src"
        </li>
        <li>
           Agora rode esse comando:
            <code>ts-node index.ts</code>
        </li>
          
   </ul>
   


    
 


