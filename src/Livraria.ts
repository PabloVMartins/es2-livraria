import Livro from './Livro'
import prompt from 'prompt-sync'
import Autor from './Autor'
import Capitulo from './Capitulo'

let teclado = prompt()


export default class Livraria {

    private livros: Array<Livro> = []


    public cadastrarLivro() {
        console.clear()
        if (this.livros.length <= 500) {
            const tituloLivro = teclado("Digite o Título do livro: ")
            const ISBN = teclado("Digite o ISNB: ")
            console.log('+----------------------------------+');
            
            const qtdAutor = +teclado("Digite o número de autores a cadastrar: ")
            const livro = new Livro(tituloLivro, ISBN)

            let nomeAutor: string;
            let dataNascimento: Date;

            for (let i = 0; i < qtdAutor; i++) {
                console.log(`Cadastro de Autor (${i + 1}/${qtdAutor}) `);               
                nomeAutor = teclado(`Digite o nome do autor: `);
                dataNascimento = new Date(teclado(`Informe a data de nascimento(aaaa/mm/dd) do autor: `).replace("/", ","));
                livro.adicionarAutor(nomeAutor, dataNascimento)
            }
            console.log('+----------------------------------+');
            const qtdCapit = +teclado("Digite o número de capítulos a cadastrar: ")

            let tituloCapitulo: string;
            let textoCapitulo: string;

            for (let i = 0; i < qtdCapit; i++) {
                console.log(`Cadastro de Capítulos (${i + 1}/${qtdCapit})`); 
                tituloCapitulo = teclado(`Digite o titulo do capítulo: `);
                textoCapitulo = teclado(`Digite o texto do capítulo: `);
                livro.adicionarCapitulo(tituloCapitulo, textoCapitulo);
            }
            console.log('+----------------------------------+');
            this.livros.push(livro)

            console.log("Livro cadastrado com Sucesso!")

        }
        else {
            return -1;
        }
    }


    public listarAcervo() {
        console.clear()

        if (this.livros.length == 0) {
            console.log("Não há livros no acervo.")
            console.log()
        }
        else {

            for (let i = 0; i < this.livros.length; i++) {
                let autores = ''
                let livro = this.livros[i]
                console.log(`[${i}] - Livro: ${livro.titulo}`)
                for (let j = 0; j< livro.Autor.length; j++) {
                    autores += livro.Autor[j].nome + ', '
                }
                autores = autores.substring(0, autores.length - 2)
                console.log(`       ISBN: ${livro.ISBN} - Autores: ${autores}`)
                
            }
        }
    }

    public modificarLivro() {
        console.clear()
        this.listarAcervo()

        if (this.livros.length > 0) {

            let id = +teclado("Digite o ID do livro: ")
            let livro = this.livros[id]

            if (id > this.livros.length-1) {
                console.log('Não há livro nesta posição do acervo')
                console.log()
                return
            }
            else {
                
                console.log('+==================================+');
                console.log('|         Livraria Jalee           |');
                console.log('+----------------------------------+');
                console.log("|1. Inserir novo capítulo          |");
                console.log("|2. Inserir novo autor             |");
                console.log("|3. Retornar                       |");
                console.log('+----------------------------------+');
                let escolha = +teclado("Escolha uma opção: ")
                
                if (escolha == 1) {
                    this.adicionaCapituloLivro(livro)
                }
                else if (escolha == 2) {
                    this.adicionaAutorLivro(livro)
                }
                else {
                    return
                }
            }
        }
    }

    public adicionaCapituloLivro(livro: Livro) {
        let tituloCapitulo = teclado(`Informe o titulo do capitulo: `);
        let textoCapitulo = teclado(`Informe o texto do capitulo: `);

        livro.adicionarCapitulo(tituloCapitulo, textoCapitulo)

        console.log("Capítulo adicionado com sucesso.")
        console.log()
    }

    public adicionaAutorLivro(livro: Livro) {
        let nomeAutor = teclado(`Informe o nome do autor: `);
        let dataNascimento = new Date(teclado(`Informe a data de nascimento(aaaa/mm/dd) do autor: `).replace("/", ","));

        livro.adicionarAutor(nomeAutor, new Date(dataNascimento))

        console.log("Autor adicionado com sucesso.")
        console.log()
    }

    public removerLivro() {
        console.clear()
        this.listarAcervo()

        if (this.livros.length > 0) {

            let id = +teclado("Digite o ID do livro desejado: ")
            console.log();

            if (id > this.livros.length-1) {
                console.log('Não há livro nesta posição do acervo')
                console.log()
                return
            }
            else {
            console.log("Livro removido com sucesso!");
            this.livros.splice(id)
            }
        }
    }

    public listarCapitulo() {
        console.clear()
        this.listarAcervo()     
        
        if (this.livros.length > 0) {

            let id = +teclado("Digite o ID do livro desejado: ")
            console.log();        
            let livro = this.livros[id]

            if (id > this.livros.length-1) {
                console.log('Não há livro nesta posição do acervo')
                console.log()
                return
            }
            else {
            this.listarCapitulos(livro)
            }
        }
    }

    public listarCapitulos(livro : Livro) {
        livro.listarCapitulos()    
    }
}
