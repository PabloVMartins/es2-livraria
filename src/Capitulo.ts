export default class Capitulo {

    constructor(
        private _titulo: string,
        private _texto: string
    ) { }

    public get titulo(): string {
        return this._titulo
    }
    
    public get texto(): string {
        return this._texto
    }
    
    public set titulo(data: string) {
        this._titulo = data;
    }
    
    public set texto(data: string) {
        this._texto = data
    }

} 
