export default class Autor {
    
    private _dataNascimento: Date
    
    constructor(
        private _nome: string,
    ) { }
       

    public get nome(): string {
        return this._nome
    }

    public get dataNascimento(): Date {
        return this._dataNascimento
    }
    
    public set nome(data: string) {
        this._nome = data;
    }
    
    public set dataNascimento(data: Date) {
        this._dataNascimento = data;
    }

} 