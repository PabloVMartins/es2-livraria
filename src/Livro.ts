import Autor from "./Autor"
import Capitulo from "./Capitulo" 

export default class Livro {
    
        private capitulos: Capitulo[]
        private autor: Autor[]

    constructor(
        protected _titulo: string,
        protected _ISBN: string
    ) { }
       
    adicionarCapitulo(titulo: string , texto: string): Number {
        
        const createdCapitulo: Capitulo = new Capitulo (titulo, texto);

        if (!this.capitulos) {
            this.capitulos = []
        }
        if (this.capitulos.length >= 99) {
            return -1;
        }
        else {
            this.capitulos.push(createdCapitulo);
            return (this.capitulos.length-1);
        }
    }

    removerCapitulo(index: number) {

        // O index recebe o numero do Capitulo, e não sua posição no array
        index = index-1

        if ((this.capitulos.length-1) >= index) {
            this.capitulos.splice(index, 1);
            return (index+1);
        }
        else {
            return -1
        }
    }

    removerAutor(nome: string) {

        const haveAutor = this.autor.findIndex((el) => el.nome == nome);

        if (haveAutor) {
            this.autor.splice(haveAutor, 1);
            return haveAutor;
        } else {
            return -1;
        }
    }

    adicionarAutor(nome: string, dataNascimento: Date): Number {

        const createdAutor: Autor = new Autor (nome);
        createdAutor.dataNascimento = dataNascimento;
        
        if (!this.autor) {
            this.autor = []
        }

        if (this.autor.length >= 6) {
            return -1;
        }
        else {
            this.autor.push(createdAutor);
            return (this.autor.length-1);
        }
    }

    public get Capitulos(): Capitulo[] {
        return this.capitulos
    }

    public get Autor(): Autor[] {
        return this.autor
    }

    public get titulo(): string {
        return this._titulo
    }

    public get ISBN(): string {
        return this._ISBN
    }

    listarCapitulos() {

        for (let i = 0; i < this.capitulos.length; i++) {
            console.log(`Capítulo ${i+1} - ${this.Capitulos[i].titulo}`);
            
        }
        console.log();        
    }
} 

