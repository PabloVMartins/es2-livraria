import Livraria from './Livraria'
import prompt from 'prompt-sync'

console.clear()

let livraria = new Livraria

let teclado = prompt()
let option: number = 0

while (option != 9) {
    

    console.log('+==================================+');
    console.log('|         Livraria Jalee           |');
    console.log('+----------------------------------+');
    console.log('|1. Cadastrar novo livro           |');
    console.log('|2. Remover livro do acervo        |');
    console.log('|3. Listar acervo                  |');
    console.log('|4. Resetar livraria               |');
    console.log('|5. Modificar Livro do Acervo      |');
    console.log('|6. Listar capítulos de livro      |');
    console.log('|9. Sair                           |');
    console.log('+----------------------------------+');
    option = +teclado("Escolha uma opção: ")    

    switch (option) {
        case 1:
            livraria.cadastrarLivro()
            break;
        case 2:
            livraria.removerLivro()
            break;
        case 3:
            livraria.listarAcervo()
            break;
        case 4:
            resetarLivraria()
            break;
        case 5:
            livraria.modificarLivro()
            break;
        case 6:
            livraria.listarCapitulo()
            break;
        case 9:
            break;
        default:
            break;
        
    }
}

function resetarLivraria() {
    console.clear()
    livraria = new Livraria()
    console.log("Acervo resetada com sucesso.")
    console.log()
}