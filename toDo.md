Engenharia de Software II
Professor  ngelo Gonçalves da Luz
Roteiro 1 - Orientação a objetos, versionamento e UML
	
Objetivos

	Este roteiro busca integrar conhecimentos obtidos na unidade curricular de Algoritmos I com conhecimentos de Engenharia de Software, com a utilização de arrays para armazenar os dados cadastrados na execução da aplicação desenvolvida. Interpretar documentações e gerar soluções é outra evidência importante desta atividade.

Lembre-se: Este roteiro não é apenas um roteiro de programação, é um roteiro também de engenharia de software.

Regras Gerais
Grupos
A atividade pode ser trabalhada em trios.

Versionamento

Um repositório deverá ser criado para a atividade e trabalhado seguindo uma organização similar a do gitflow, respeitando rigorosamente o seguinte:

Branch master ou main: Branch protegida que só receberá o código no dia da apresentação do trabalho, na sua versão final.

Branch dev: Branch protegida que somente receberá código através de MERGE REQUEST, que podem seguir o modelo que segue no final deste documento.

Branch feature/* : São as branchs protegidas criadas a partir da dev e todos os integrantes somente enviarão código para a dev através de MR.

Todo o grupo deve ter o mesmo padrão de commits.

Todos commits devem ser em inglês.

MR em português.

Todos os integrantes devem ter MRs no repositório.

Aplicação
Deverá ser desenvolvida com Typescript, utilizando boas práticas de Orientação a objetos.

Somente a classe "Livraria" deverá ter entrada e saída de dados.

A aplicação deverá estar o mais próxima possível da documentação e do diagrama de classes.

Apresentação
Dia 8/4 os grupos devem apresentar, em 15 minutos as suas soluções para a FASE 1, apresentando a solução em código, estratégias que foram adotadas e o histórico do repositório (commits, MR, etc…), o mesmo deverá ser feito na semana seguinte com relação a FASE 2, dia 15/4.

A avaliação será dada em um acumulado entre entregado do grupo e individual.
		
# Fase 0 - Mapeamento de funcionalidades
Realizar a leitura das fases 1 e 2 e mapear os casos de uso e/ou requisitos a fim de facilitar a organização para execução do projeto. 

Este mapeamento deverá estar contemplado na wiki ou boards do projeto.
# Fase 1 - Projeto de classes

Desenvolver uma classe Autor. que possui um nome e data de nascimento (do tipo Date). O construtor deve forçar o preenchimento do nome do Autor. 

Se liga! As regras de encapsulamento e boas práticas devem ser aplicadas para todas as classes deste projeto.
	
	Desenvolver uma classe Capítulo, que representa um Capítulo de um livro. Esse capítulo possui um título e um texto. Deve possuir um construtor conforme julgue mais apropriado para a solução do problema.
	
	Desenvolver uma classe Livro, que possui um título, um ISBN, vários capítulos e um ou vários autores. O construtor deve obrigar o preenchimento do título e do ISBN do livro. Esta classe deve possuir alguns métodos além dos métodos tradicionais, e são eles: 
	
	adicionarCapitulo - que receberá por parâmetro o título e o texto do capítulo, e retornar um inteiro com a posição do array de capítulos onde o capítulo foi inserido, ou -1, caso não tenha sido possível inserir. Considere que um livro possui no máximo 100 capítulos, caso precise especificar esta informação. 	
	
	removerCapitulo - que recebe por parâmetro um Capítulo, e remove o mesmo do array de capítulos. Retornando a posição do array onde estava o Capítulo ou -1 caso o capítulo não for encontrado.
	
	removerAutor - que recebe por parâmetro um Autor e o remove do array de autores, retornando a posição onde estava o autor ou -1 caso não tenha sido possível remover.
	
	adicionarAutor - recebe por parâmetro um Autor e o adiciona no array de autores, retornando a posição do array onde o autor foi inserido ou -1 caso não tenha sido possível inseri-lo. Considere 	que um livro possa possuir no máximo 6 autores.
	
# Fase 2 - Criar a classe de interação com o usuário

Desenvolver uma classe Livraria, que será a classe que conterá toda a entrada e saída de dados, essa classe deve possuir um array de Livros, e vários métodos privados para melhor organizar o código, tais como: 
	
	cadastrarLivro - que cadastra um novo livro e retorna a posição 	que este foi inserido ou -1 caso não possa ser cadastrado. Considere que a livraria pode conter até 500 livros. Deverá utilizar os métodos “cadastrarAutores” e “cadastrarCapitulos”, descritos a seguir.
	
	cadastrarAutores – que é um método auxiliar que será utilizado pelo “cadastrarLivro”, centralizando as informações referentes ao cadastro de autor. Este receberá como parâmetro um livro e o número de autores que será cadastrado. Esse método irá cadastrar valores em um dos livros já cadastrados na livraria, que deve ser 	selecionado previamente. 	
	CadastrarCapitulos - deve funcionar da mesma forma que o “cadastrarAutores”, porém com a funcionalidade voltada para Capítulos. Um exemplo de fluxo de cadastro pode ser visto na Figura 1.
	
	
Figura	1. Exemplo de fluxo de cadastro de livro

	
	
	listarAcervo - Deverá apresentar todas as obras cadastradas. Mostrando também a posição em que ela se encontra no array. Um exemplo de listagem pode ser visto na Figura 2.
	
Figura 	2. Exemplo de listagem de livro
	
	modificarLivroDoAcervo - Deverá dar duas opções ao usuário: Adicionar Autor a um livro ou adicionar capítulo. Após a escolha, 	deverá ser utilizado um dos métodos descritos a seguir, “adicionarAutorLivro” ou “adicionarCapituloLivro”. Um exemplo pode ser visto na Figura 3.
	
	 		
		
Figura 3. Exemplo de fluxo de edição de livro cadastrado

	
	adicionarAutorLivro - Funciona da mesma forma que o “adicionarCapituloLivro".
	
	adicionarCapituloLivro - deve receber por parâmetro um livro, que será um livro selecionado previamente dos livros já cadastrados e irá inserir novos capítulos neste livro.
	
	removerLivro - Deverá remover um livro da livraria. Ver Figura 4. 	Além do funcionamento visto, em todos os casos podem ser utilizadas mensagens de confirmação das ações selecionadas. 		
		
Figura	4. Exemplo de remoção de livro do acervo

	
	listarCapitulos - que será utilizado apenas para fazer a descoberta do livro que deve ser utilizado. Após a descoberta, o  “listarCapitulos” descrito a seguir deve ser invocado.

	listarCapitulos - Deve receber um livro por parâmetro e listar os títulos dos capítulos do mesmo. Exemplo de funcionamento pode ser visto na Figura 5.
	
Figura 5. Exemplo de listagem de capítulos do livro
	
	resetarLivraria - remover todos os livros cadastrados.
	
	menu – deve centralizar as chamadas dos métodos do menu principal. Deve seguir uma implementação similar à da Figura 6, só que em Typescript, o modelo está em java.	 		
		
Figura 6. Código do Menu principal
	
Informações adicionais
A Figura 7 apresenta um diagrama de classes para o projeto proposto.
	

Figura 	7. Diagrama de classes do projeto proposto


Modelo para Merge Request
# Descrição
Adiciona módulo de mensagens com remetente, destinatário e vinculado a uma solicitação.

## Implementa
- Criação de mensagens
- Listagem de mensagens por solicitação
- Listagem de mensagens por usuários
- Adição de subscription
- Demais queries e mutations do nest-query

## Ajusta
- Módulo de Users para listar mensagens
- Módulo de Solicitations para listar mensagens

## Corrige
- Nada relevante

## Fecha
Não existia uma Issue sobre mensagens.

## Tipo de mudança
- [x] Nova Funcionalidade
- [ ] Correções de bugs
- [x] Ajuste

# Como foi testado?
Testes manuais no insomnia.

### Exemplo:
#### Criação de mensagem
![image](/uploads/ac1555613c825bc53a30527392427c72/image.png)

#### Listagem de usuários com mensagens enviadas e recebidas
![image](/uploads/f8ea55b3ed3afb23514277605e241455/image.png)

#### Listando mensagens de solicitações
![image](/uploads/d6eda730e9a38cf1e31486ec7f9535be/image.png)


Checklist:
- [x] Meu código segue os padrões do repositório, como gitmoji e commits organizados e com mensagens adequadas
- [x] Meu código segue os padrões deste projeto
- [ ] Escrevi testes automatizados necessários para esta entrega
- [ ] Testei exaustivamente a entrega
- [x] Revisei o código que está sendo entregue
- [x] Documentei de alguma forma o que foi feito para auxiliar próximos devs (em código, insomnia, documentos auxiliares)
- [x] Minhas mudanças não geram nenhum motivo de alerta ou possível quebra de outras funcionalidades

# Informações adicionais:
- Link para o Miro:https://miro.com/app/board/o9J_laTuwRs=/

Material de apoio

Conhecendo o typescript - https://www.youtube.com/watch?v=MxVNWwhE_Cs&list=PLDqnSpzNKDvnks4WTlOwAoL9Lw9imUeoO

Playlist de orientação a objetos - https://www.youtube.com/watch?v=G5TOitIft8o&list=PLDqnSpzNKDvnh-0RCYbIL5WzGCCFV1Ghm

Playlist de versionamento - 
https://www.youtube.com/watch?v=GIEquFr3jcg&list=PLDqnSpzNKDvkfF_ZMfukmOG3MtGKfXlfJ

Playlist de organização de projetos no gitlab - https://www.youtube.com/playlist?list=PLDqnSpzNKDvmeJY64RTtgjR9TDvDz8Rf0


